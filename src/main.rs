use std::io::stdin;

fn scan_i32(max: i32) -> i32 {
    let mut str_num = String::new();
    loop {
        stdin().read_line(&mut str_num).unwrap();
        match str_num.trim().parse::<i32>() {
            Err(_) => {
                println!("Enter a valid number!");
            }
            Ok(n) if n > max => {
                println!("Number should be between 0 and {max}!");
            }
            Ok(n) => break n,
        }
        str_num.clear();
    }
}

enum Grade {
    A,
    B,
    C,
    D,
    F,
}

impl std::fmt::Display for Grade {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::A => "A",
                Self::B => "B",
                Self::C => "C",
                Self::D => "D",
                Self::F => "F",
            }
        )
    }
}

fn main() {
    println!("Enter marks for first quiz: ");
    let quiz = scan_i32(10);
    println!("Enter marks for first midterms: ");
    let midterm1 = scan_i32(20);
    println!("Enter marks for second midterms: ");
    let midterm2 = scan_i32(20);
    println!("Enter marks for final exams: ");
    let finaln = scan_i32(25);
    println!("Enter marks for lab evaluation: ");
    let lab = scan_i32(20);
    println!("Enter marks for class participations: ");
    let cp = scan_i32(5);

    let total = quiz + midterm1 + midterm2 + finaln + lab + cp;

    println!("Total marks: {}", total);

    let grade = if total >= 90 {
        Grade::A
    } else if total >= 80 {
        Grade::B
    } else if total >= 70 {
        Grade::C
    } else if total >= 60 {
        Grade::D
    } else {
        Grade::F
    };

    println!("Grade: {grade}");
}
